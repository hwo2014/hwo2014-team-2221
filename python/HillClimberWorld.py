import copy
import random
import math
from HillClimberBot import HillClimberBot
import os
import sys
from datetime import datetime
import socket
import time
import json
import threading

class HillClimberWorld(object):
    """
    hill blimbing
    """
    _iterations = 100
    _used_ids = []
    def __init__(self, host, port, name, key, name_prefix = 'abcd', load_from_file = False, file_name = None):
        if load_from_file:
            input_file = open(file_name)
            data = json.load(input_file)
            # get stored data 
            genome = data['genome']
            key = data['key']
            name = data['name']
            self.bot = HillClimberBot(host, int(port), name, key, False)
            self.bot.genome = genome
            print(genome)
        else:
            self.bot = HillClimberBot(host, int(port), str(os.getpid()%71) + name , key, True)
        self.tracks = ["germany", "keimola", "france", "usa"]
    
    def update_mutations(self): 
        self._mutation_bank = [     # the possible mutations
            -self._acceleration * self._acceleration,
            self._acceleration * self._acceleration,
            self._acceleration,   # wolverine
            -self._acceleration,   # cyclpse
            0,   #mystique
            1 / float(self._acceleration), # jaggernaut
            -1/ float(self._acceleration),  # magneto
            1/ float(self._acceleration * self._acceleration),
            -1/float(self._acceleration * self._acceleration)
        ]
        
    def _mutate(self, bot, index, func):
        bot.genome[index] += bot.genome[index] * func

    def climb(self):
        count = 0
        # should add more termination parameters
        while count < self._iterations:
            self._acceleration = 2.5
            self.update_mutations()
            print("%s iteration" % str(count))
            print(self._mutation_bank)
            for i in range(0, len(self.bot.genome)):
                print("%s parameter" % str(i))
                best = 99999999999
                self.scores = [ [best for x in range(0, len(self._mutation_bank))] for y in range(0, len(self.tracks))]
                print(self.bot.genome)
              
                # log the state of the evolution and their scores
                timeStamp = str(datetime.now().time()) + str(count) + "-iteration" + str(i)
                outputFile = open(timeStamp, 'wb')
                d = self.bot.__dict__
                generator = d['generator']
                stop = d['_stop']
                del d['generator']
                del d['_stop']   
                json.dump(d, outputFile)
                d['generator'] = generator
                d['_stop'] = stop
                outputFile.close()
                self.threads = []
                for w in range(0, len(self.tracks)):
                    self._launch_batch(w, i, count)
               
                for y in range(0, len(self.tracks)):
                    for x in range(0, len(self._mutation_bank)):
                        print("starting thread %s" % str(x))
                        self.threads[(y * len(self._mutation_bank)) + x].start()
                    time.sleep(65)
                for x in range(0, len(self.tracks) * len(self._mutation_bank)):
                    self.threads[x].join()

                # update current best
                average = [sum(self.scores[i])/float(len(self.scores[i])) for i in range(0, len(self.tracks))]
                if min(average) < best:
                    print("best score %s" % str(min(average)))
                    self._mutate(self.bot, i, self._mutation_bank[average.index(min(average))])
                else:
                # random walk?
                    pass
            count += 1

    def _launch_batch(self, track, i, count):
        bots = []
        for j in range(0, len(self._mutation_bank)):        
            bots.append(HillClimberBot(self.bot.host, self.bot.port, self.bot.name + str(track) + "-" + str(count)+ "-" + str(i) + "-" +str(j), self.bot.key, True))
            bots[j].genome = self.bot.genome[0:len(self.bot.genome)]
            print("launch time %s" % str(datetime.now().time()))
            self._mutate(bots[j], i, self._mutation_bank[j])
            t = threading.Thread(target=self._launch_track, args = (bots, bots[j], self.tracks[track], j, track))
            self.threads.append(t)
 
    def _launch_track(self, bots, bot, country, j, track):
        bot.reconnect()
        i = random.randint(0, 999999999)
        while i in self._used_ids:
            i = random.randint(0, 99999999)
        self._used_ids.append(i)
        password = str(os.getpid()) + "cadriximba" + str(i)
        #create the race before others join
        bot.create_race(country, password, 1)
        bot.test_race(country, password, 1)

        if hasattr(bot, 'result_average_millis'):
            self.scores[track][j] = bot.result_average_millis
        elif hasattr(bot, 'result_best_lap_millis'):
            self.scores[track][j] = bot.result_best_lap_millis
        self.scores[track][j] += bot.crash_count * 10

        t = datetime.now() - bot.start_time
        if bot.stopped():
            print("bot %s is stopping the rest" % str(j))
        print("%s quick stopping" % str(j))
        for x in range(0, len(self._mutation_bank)):
            if bots[x].start_time != None and datetime.now() - bots[x].start_time > t:
                bots[x].stop()
