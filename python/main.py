import sys
import socket
from GeneticBotWorld import GeneticBotWorld
from HillClimberWorld import HillClimberWorld
from NoobBot import NoobBot
from PhysicsBot import PhysicsBot

if __name__ == "__main__":
    if len(sys.argv) < 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        f = sys.argv[5] if len(sys.argv) == 6 else None
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
# genetic bot
#        botWorld = GeneticBotWorld(host, int(port), name, key, 20, 'tomtom', False, None)
#        botWorld.evolve()
# hill climber
        load_file = True if f != None else False
        botWorld = HillClimberWorld(host, int(port), name, key, 'tommy', load_file, f)
        botWorld.climb()

# noob bot

#        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#        s.connect((host, int(port)))
#        bot = NoobBot(s, name, key)
#        bot.run()

# physics bot
#        bot = PhysicsBot(host, int(port), name, key)
#        bot.reconnect()
#        bot.run()

