import json
import socket
import sys
from Sensor import Sensor

class PhysicsBot(object):
    turbo_on = False
    turbo_available = False
    turbo_factor = 0
    turbo_tick_count = 0
    crash_count = 0

    def __init__(self, host, port, name, key):
        self.name = name
        self.key = key
        self.done = False
        self.host = host
        self.port = port
        self.sensor = Sensor()
    
    def reconnect(self):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect((self.host, self.port))

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.sendall(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})
    
    def create_race(self, track_name, password, number_of_bots):       
        self.msg ( "createRace", {"botId": {"name": self.name,
                                             "key": self.key
                                           },
                                 "trackName": track_name,
                                 "password": password,
                                 "carCount": number_of_bots })
    def on_game_end(self, data):
        print("Race ended")
        sys.stdout.flush()
        for result in data['results']:
            if result['car']['name'] == self.name and result['car']['color'] == self.color:
                if 'laps' in result['result']:
                    self.result_laps = result['result']['laps']
                    self.result_ticks = result['result']['ticks']
                    self.result_millis = result['result']['millis']
                    self.result_average_millis = (result['result']['millis'] / \
                                          float(result['result']['laps']))
        for result in data['bestLaps']:
            if result['car']['name'] == self.name and result['car']['color'] == self.color:
                if 'lap' in result['result']:
                    self.result_best_lap = result['result']['lap']
                    self.result_best_lap_ticks = result['result']['ticks']
                    self.result_best_lap_millis = result['result']['millis']
        self.done = True
        self.ping()

    def test_race(self, track_name, password, number_of_bots):
        print("joining test race on track %s with %s bots" % (track_name, str(number_of_bots)))
        self.msg("joinRace", {"botId": { "name": self.name,
                                            "key": self.key
                                         },
                                "trackName": track_name,
                                "password" : password,
                                "carCount": number_of_bots })
        self.msg_loop()

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def switchLane(self, lane):
        #print("switching lane to %s" % lane)
        #self.msg("switchLane", lane)
        self.ping()
    
    def turbo(self):
        #print("applying turbo")
        #self.msg("turbo", {})
        self.ping()

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_turbo_available(self, data):
        self.turbo_available = True
        self.turbo_tick_count = data["turboDurationTicks"]
        self.turbo_factor = data["turboFactor"]

    def _turbo_tick(self):
        self.turbo_tick_count -= 1
        if self.turbo_tick_count == 0:
            self.turbo_factor = 0
            self.turboAvailable = False

    def on_join(self, data):
        print("%s Joined" % self.name)
        sys.stdout.flush()
        self.ping()

    def on_your_car(self, data):
        if self.name != data['name']:
            print("bot names don't correspond %s Vs %s" % (self.name , data['name']))
            sys.stdout.flush()
            sys.exit(1)
        self.color = data['color']

    def on_game_init(self, data):
        print("%s game inititalised" % self.name)
        self.race = data['race']
        #cache the dimensions
        for car in self.race['cars']:
            if car['id']['name'] == self.name and car['id']['color'] == self.color:
                self.width = car['dimensions']['width']
                self.length = car['dimensions']['length']
                self.guide_flag_position = car['dimensions']['guideFlagPosition']

    def on_game_start(self, data):
        print("%s Race started" % self.name)
        sys.stdout.flush()
        self.ping()

    def on_car_positions(self, data):        
        self.sensor.log()
        self.sensor.sense(self, data)
        t = 0.5
        if self.sensor.straight_to_straight_to_straight:
            t = 1
        elif self.sensor.straight_to_straight_to_curve:
            t = 0.6
        elif self.sensor.curve_to_straight_to_straight:
            t = 0.8
        elif self.sensor.curve_to_straight_to_curve:
            t = 0.6
        elif self.sensor.curve_to_curve_to_straight:
            t = 0.6
        elif self.sensor.curve_to_curve_to_curve:
            t = 0.5
        elif self.sensor.straight_to_curve_to_straight:
            t = 0.6
        elif self.sensor.straight_to_curve_to_curve:
            t = 0.7
        self.throttle(t)
        self.sensor.set_throttle(t)
    
    def get_done_state(self):
        return self.done

    def on_crash(self, data):
        print("%s crashed" % data['name'])
        if data['name'] == self.name and data['color'] == self.color:
            self.turbo_tick_count = 0
            self.turbo_available = False
            self.turbo_factor = 1
        sys.stdout.flush()
        self.ping()
        self.crash_count += 1

    def on_error(self, data):
        print("Error: {0}".format(data))
        sys.stdout.flush()
        self.ping()

    # in case we hit the thread safety problems
    def _read_line(self):
        ret = ''

        while True:
            c = self.socket.recv(1)

            if c == '\n':
                break
            else:
                ret += c

        return ret

    def msg_loop(self):
        print("starting message loop")
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'turboAvailable': self.on_turbo_available,
            'yourCar': self.on_your_car,
            'gameInit': self.on_game_init,
        }
        socket_file = self.socket.makefile()
        line = socket_file.readline()       
        #line = self._read_line()
        while line:
            msg = json.loads(line)
            if self.turbo_available:
                self._turbo_tick
            
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                sys.stdout.flush()
                self.ping()
            if msg_type == 'tournamentEnd' or msg_type == "dnf":
                break
            #line = self._read_line()
            line = socket_file.readline()
        print("finished msg_loop")
