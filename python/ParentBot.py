from datetime import datetime
import json
import socket
import sys
import threading

class ParentBot(object):
    turbo_on = False
    turbo_available = False
    turbo_factor = 0
    turbo_tick_count = 0
    can_switch = False
    straight_to_straight = False
    straight_to_curve = False
    curve_to_straight = False
    curve_to_curve = False
    previous_throttle = 0
    crash_count = 0
    start_time = None
    tick = -1
    def __init__(self, host, port, name, key):
        self.name = name
        self.key = key
        self.done = False
        self.host = host
        self.port = port
        self._stop = threading.Event()

    def stop(self):
        self._stop.set()

    def stopped(self):
        return self._stop.isSet()
    
    def reconnect(self):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect((self.host, self.port))

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.sendall(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})
    
    def create_race(self, track_name, password, number_of_bots):       
        self.msg ( "createRace", {"botId": {"name": self.name,
                                             "key": self.key
                                           },
                                 "trackName": track_name,
                                 "password": password,
                                 "carCount": number_of_bots })
        

    def test_race(self, track_name, password, number_of_bots):
        print("joining test race on track %s with %s bots" % (track_name, str(number_of_bots)))
        self.msg("joinRace", {"botId": { "name": self.name,
                                            "key": self.key
                                         },
                                "trackName": track_name,
                                "password" : password,
                                "carCount": number_of_bots })
        self.msg_loop()

    def throttle(self, throttle):
        self.previous_throttle = throttle
        self.msg("throttle", throttle)

    def switchLane(self, lane):
        #print("switching lane to %s" % lane)
        #self.msg("switchLane", lane)
        self.ping()
    
    def turbo(self):
        #print("applying turbo")
        #self.msg("turbo", {})
        self.ping()

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_turbo_available(self, data):
        self.turbo_available = True
        self.turbo_tick_count = data["turboDurationTicks"]
        self.turbo_factor = data["turboFactor"]

    def _turbo_tick(self):
        self.turbo_tick_count -= 1
        if self.turbo_tick_count == 0:
            self.turbo_factor = 0
            self.turboAvailable = False

    def on_join(self, data):
        print("%s Joined" % self.name)
        sys.stdout.flush()
        self.ping()

    def on_your_car(self, data):
        if self.name != data['name']:
            print("bot names don't correspond %s Vs %s" % (self.name , data['name']))
            sys.stdout.flush()
            sys.exit(1)
        self.color = data['color']

    def on_game_init(self, data):
        print("%s game inititalised" % self.name)
        self.race = data['race']
        #cache the dimensions
        for car in self.race['cars']:
            if car['id']['name'] == self.name and car['id']['color'] == self.color:
                self.width = car['dimensions']['width']
                self.length = car['dimensions']['length']
                self.guide_flag_position = car['dimensions']['guideFlagPosition']

    def on_game_start(self, data):
        self.start_time = datetime.now()
        print("%s Race started" % self.name)
        sys.stdout.flush()
        self.ping()

    def on_car_positions(self, data, tick):        
        # turbo data
        self.tick = int(tick)
        for car in data:
            if car['id']['name'] == self.name and car['id']['color'] == self.color:
                car["turboAvailable"] = self.turbo_available
                car["turboFactor"] = self.turbo_factor
                car["turboTickCount"] = self.turbo_tick_count
                pieces = self.race['track']['pieces']
                car_piece_index = car['piecePosition']['pieceIndex']
                if "switch" in pieces[car_piece_index] and pieces[car_piece_index]['switch']:
                    if car['piecePosition']['lane']['startLaneIndex'] == car['piecePosition']['lane']['endLaneIndex']:
                        if car['piecePosition']['lane']['startLaneIndex'] < len(self.race['track']['lanes']) -1:
                            self.switch_right = True
                        else:
                            self.switch_right = False
                        if car['piecePosition']['lane']['startLaneIndex'] >0:
                            self.switch_left = True
                        else:
                            self.switch_left = False
                        self.can_switch = True
                    else:
                        self.can_switch = False 
                        self.switch_left = False
                        self.switch_right = False
                else:
                    self.can_switch = False
                    self.switch_left = False
                    self.switch_right = False
                car["canSwitch"] = self.can_switch

                # lane of the car
                self.start_lane_index = car['piecePosition']['lane']['startLaneIndex']
                self.end_lane_index = car['piecePosition']['lane']['endLaneIndex']
                    
                # determine the position of the car
                next_index = (car_piece_index + 1) % len(pieces)
                if "length" in pieces[car_piece_index]:
                    self.straight_to_straight = True if "length" in pieces[next_index] else False
                    self.straight_to_curve = True if "radius" in pieces[next_index] else False
                else:
                    self.straight_to_straight = False
                    self.straight_to_curve = False
                if "radius" in pieces[car_piece_index]:
                    self.curve_to_curve = True if "radius" in pieces[next_index] else False
                    self.curve_to_straight = True if "length" in pieces[next_index] else False
                else:
                    self.curve_to_curve = False
                    self.curve_to_straight = False    
                    
    
    def get_done_state(self):
        return self.done

    def on_crash(self, data):
        print("%s crashed" % data['name'])
        if data['name'] == self.name and data['color'] == self.color:
            self.turbo_tick_count = 0
            self.turbo_available = False
            self.turbo_factor = 1
        sys.stdout.flush()
        self.ping()
        self.crash_count += 1

    def on_error(self, data):
        print("Error: {0}".format(data))
        sys.stdout.flush()
        self.ping()

    def on_dnf(self, data):
        print("%s got disqualified because %s" % (data['car']['name'], data['reason']))

    def on_lap_finished(self, data):
        print("%s finished lap" % data['car']['name'])

    # in case we hit the thread safety problems
    def _read_line(self):
        ret = ''

        while True:
            c = self.socket.recv(1)

            if c == '\n':
                break
            else:
                ret += c

        return ret

    def msg_loop(self):
        print("starting message loop")
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'turboAvailable': self.on_turbo_available,
            'yourCar': self.on_your_car,
            'gameInit': self.on_game_init,
            'lapFinished': self.on_lap_finished,
            'dnf': self.on_dnf,
        }
        socket_file = self.socket.makefile()
        line = socket_file.readline()       
        #line = self._read_line()
        while line:
            msg = json.loads(line)
            if self.turbo_available:
                self._turbo_tick
            
            msg_type, data = msg['msgType'], msg['data']
            if msig_type in msg_map:
                if msg_type = "carPositions":
                    msg_map[msg_type](data, msg["gameTick"])
                else:
                    msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                sys.stdout.flush()
                self.ping()
            if msg_type == 'tournamentEnd' or msg_type == "dnf":
                break
            #line = self._read_line()
            line = socket_file.readline()
 
            if self.start_time != None and (datetime.now() - self.start_time).seconds > 60 * 60:
               print("%s took too long" % self.name)
               break
            if  self.stopped():
               print("%s was stopped" % self.name)
               break

        print("finished msg_loop")
