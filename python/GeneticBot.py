import json
import socket
import sys
from ParentBot import ParentBot
from InputGenerator import InputGenerator
import random

class GeneticBot(ParentBot):

    _scale_factor = [ 71, 71, 91, 91]

    def __init__( self, host, port, name, key, randomize = True):
        super(GeneticBot, self).__init__(host, port, name, key)
        self.generator = InputGenerator()
        # 10 here is an arbitray number
        if randomize:
            self.genome = None
        else:
            self.genome = []

    def setup(self, socket, name, key):
        self.name = name
        self.socket = key

    def get_parent_dict(self):
        return super(GeneticBot, self).get_dict()

    def get_genome(self):
        return self.genome

    def set_genome(self, genome):
        if len(self.genome) != len(genome):
            print "genomes have different lengths"
            sys.stdout.flush()
            sys.exit(1)

        for i in range(0, len(genome)):
            self.genome[i] = genome[i]
    
    def set_gene(self, index, value):
        self.genome[index] = value

    def get_gene(self, index):
        return self.genome[index]

    # generate the input from each tick
    def on_car_positions(self, data):
        super(GeneticBot, self).on_car_positions(data)

        generatedInput = self.generator.generate(self, data)
        self.generator.set_previous_throttle(self.throttle)
        throttle = self.apply_genomes(generatedInput)
        
        # need to determine if switch or turbo are applicable
        self.throttle(throttle)

    # this should get the scores and associate them with the bot
    # so that we can evolve them
    def on_game_end(self, data):
        print("Race ended")
        sys.stdout.flush()
        for result in data['results']:
            if result['car']['name'] == self.name and result['car']['color'] == self.color:
                self.result_laps = result['result']['laps']
                self.result_ticks = result['result']['ticks']
                self.result_millis = result['result']['millis']
                self.result_average_millis = (result['result']['millis'] / \
                                         float(result['result']['laps']))
        self.done = True
        self.ping()

    def set_score(self, score):
        self.result_laps, self.result_ticks, self.result_millis, self.result_average_millis = score

    # this is where we apply the genomes to our input
    def apply_genomes(self, generated_input):
        # cap the values between 0 and 1
        # if not initialised, set it to a value such that we start with 0.5 throttle
        if self.genome == None:
            self.genome = [ 1 for i in range(0, len(generated_input))]
            for i in range(0, len(generated_input)):
                if generated_input[i] == 0:
                    self.genome[i] = 1
                else:
                    self.genome[i] = 0.5 / generated_input[i]

        product = [ self.genome[i] * generated_input[i] for i in range(0, len(generated_input))]
        
        s = 0.5
        if self.straight_to_straight:
            s = sum(product[0:5])    
        elif self.straight_to_curve:
            s = sum(product[5:13])
        elif self.curve_to_straight:
            s= sum(product[13:21])
        elif self.curve_to_curve:
            s = sum(product[21:29])
       
        s = min(1, max( 0, s))
        return s
