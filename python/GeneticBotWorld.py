import random
import math
import threading
from GeneticBot import GeneticBot
from operator import attrgetter
import os
import sys
from datetime import datetime
import time
import json

class GeneticBotWorld(object):
    """
    this handles all the genetic stuff
    makes the bots, tests and evolves them
    it records the details of the evolution so that things can be picked up
    from any arbitrary point

    """
    _mutation_probability = 1 / float(15) # bullsit arbitrary numbers
    _mutation_acceleration = 10 # more bullshit arbitrary numbers
    _max_parents = 4 # maximum number of parents used for crossing
    _used_ids = []

    def __init__(self, host, port, name, key, number_of_bots = 80, name_prefix = 'abcd', load_from_file = False, file_name = None):

        self.bot_farm = []

        if load_from_file:
            input_file = open(file_name)
            jdata = json.load(input_file)

            self.number_of_bots = len(jdata)
            for i in range(0, number_of_bots):
                # get stored data
                _data = jdata[i]
                _genome = _data['genome']
                _key = _data['key']
                _name = _data['name']
                _score = [_data['result_laps'], _data['result_ticks'], _data['result_millis'], _data['result_average_millis']]
                
                _bot = GeneticBot(host, int(port), _name, _key, False)
                _bot.set_genome(_genome)
                _bot.set_score(_score)
                self.bot_farm.append(_bot)

        else:
            self.number_of_bots = number_of_bots
            for i in range(0, number_of_bots):
                _bot = GeneticBot(host, port, str(os.getpid()) +name + str(i), key, True)
                self.bot_farm.append(_bot)
        
        self._mutation_bank = [     # the possible mutations
            self._accelerate,   # wolverine
            self._decelerate,   # cyclpse
            self._do_nothing,   #mystique
            self._reciprocal_accelerate, # jaggernaut
            self._reciprocal_decelerate  # magneto
        ]

    def evolve(self):
        print("evolving")
        count = 0
        while count < 20:
            print("Currently on %s generation" % str(count +1) )
            # run the races for all the fuckers and score the bots
            if self._race():

                #select the good parents
                parents = self._select()

                # generate the next generation
                # currently keeping the entire set of parents, 
                # unmutated and uncrossed, need to thinnk that through
                new_bot_farm = parents
                while len(new_bot_farm) < self.number_of_bots:
                    new_bot_farm.append(self._cross(parents))
                self.bot_farm = new_bot_farm

                # log the state of the evolution and their scores
                timeStamp = str(datetime.now().time())
                outputFile = open(timeStamp, 'wb')

                jdata = {}

                for i in range(0, len(self.bot_farm)):
                    jdata[i] = self.bot_farm[i].__dict__

                json.dump(jdata, outputFile)
                outputFile.close()
                count += 1
 
    # run the races
    def _race(self):
        print("running the races")
        num = len(self.bot_farm)
        i = 0
        pack = 4
        ran = 0
        while ran < num:
            i = ran
            # start set in finland
            #ran = self._launch_track( ran, min(pack, num-ran), "keimola")

            # start set in germany
            #ran = self._launch_track( ran, min(pack, num-ran), "germany")

            # start set in usa
            ran = self._launch_track( ran, min(pack, num - ran), "usa")

            # poll all running
            done = 0
            all_done = False
            while not all_done:
                for j in range(i, ran):
                    if self.bot_farm[j].get_done_state() == False:
                        all_done = False
                        break
                    else:
                        print "bot " + str(j) + " is done"
                        sys.stdout.flush()
                        all_done = True

            print("The whole pack is done")
            sys.stdout.flush()
        return True
    
    def _launch_track(self, ran, pack, country):
        i = random.randint(0, 999999999)
        while i in self._used_ids:
            i = random.randint(0, 99999999)
        self._used_ids.append(i)
        password = str(os.getpid()) + "cadriximba" + str(i)
        #create the race before others join
        self.bot_farm[ran].reconnect()
        self.bot_farm[ran].create_race(country, password, pack)
        for j in range(ran, ran + pack):
            print("starting bot %s in usa" % str(j))
            if j != ran:
                self.bot_farm[j].reconnect()
            t = threading.Thread(target=self.bot_farm[j].test_race, args=(country, password, pack))
            t.start()
        ran = ran + pack
        return ran


    # selection criteria for the next generation parents?
    # select the good candidates for the next generation
    def _select(self):
        print("selecting parents for next generation")
        sys.stdout.flush()
    # sort the bots in order and pick the eigth and probably other random blokes      
        sorted_farm = sorted( self.bot_farm, key = attrgetter('result_average_millis'))
        # use the top quarter of best performers
        selected = sorted_farm[0 : int( math.ceil(len(self.bot_farm) / float(4)))]

        # use a third of the generation to make the next generation
        # 10% chance of the poor performers getting in
        while len(selected) < (1/float(3)) * len(self.bot_farm):
            for i in reversed(range(len(selected), len(self.bot_farm))):
                if random.random() < 1/float(10):
                    selected.append(bot_famr[i])

        return selected

    # mutate the dudes
    def _mutate(self, bot):
        print("mutating the offspring")
        sys.stdout.flush()
        index = int(round(random.random() * (len(bot.get_genome()-1))))
        mutation_index = int(round(random.random() * (len(_mutation_bank) -1)))
        bot.set_gene(index, _mutation_bank[mutation_index](bot, index))


    # here are the mutations of the top of my head
    # they probably need testing
    def _accelerate(self, bot, index):
        return (bot.get_genome())[index] * _mutation_acceleration

    def _decelerate(self, bot, index):
        return (bot.get_genome())[index] * (- _mutation_acceleration)

    def _do_nothing(self, bot, index):
        return (bot.get_genome())[index]

    def _reciprocal_accelerate(self, bot, index):
        return (bot.get_genome())[index] * (1 / float(_mutation_acceleration))

    def _reciprocal_decelerate(self, bot, index):
        return (bot.get_genome())[index] * (-1/ float(_mutation_accelerateion))

    # some baby making time :)
    def _cross(self, parents):
        # 2 < number_of_parents <= 4
        number_of_parents = _min_parents + int( round( random.random() * (_max_parents - _min_parents)))
        # choose arbitrary parents
        chosen_parents = []
        genome_length = len(parents[0].get_genome())
        for i in range(0, number_of_parents):
            chosen_parents.append(parents[int(round(random.random() * (number_of_parents-1)))])
        
        # select how we intend to mix up the parentis
        # this picks the indices
        number_of_indices = number_of_parents - 1

        # atlease 1 gene from each parent
        indices = [0]
        for i in range(1, number_of_indices):
            indices.append( indices[i-1] + 1 + #at least 1 gene from each parent 
                            int( round( random.random() * 
                                        (genome_length -(number_of_parents - i -1)))))
        indices.append(genome_length)

        # this does the actual crossing
        child_bot = GeneticBot()
        for j in range(0, len(indices) -1):
            for i in range(indices[j], indices[j+1]):
                child_bot.set_gene(i, chosen_parents[0].get_gene(i))

        # should we do some mutation? 
        if random.random() < _mutation_probability:
            self._mutate(child_bot)
