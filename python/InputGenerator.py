import sys
import math
from InputSensor import InputSensor

class InputGenerator(object):

    _input_values = [0 for i in range(0, 41)]
       #straight to straight
       #0 linear velocity                         straight to straight
       #1 linear acceleration
       #2 angle of car
       #3 length of stretch
       #4 previous throttle
       #5 delta throttle
       
       #straight to curve
       #6 linear velocity
       #7 linear acceleration
       #8 drift angular velocity
       #9 drinft angular acceleration
       #10 drift angle of car
       #11 angle of curve
       #12 radius of curve
       #13 distance to curve
       #14 previous throttle
       #15 delta throttle
       

       #curve to straight
       #16 linear velocity
       #17 linear acceleration
       #18 drift angular velocity
       #19 drift_angular_acceleration
       #20 drift angle of car
       #21 angle of curve
       #22 radius of curve
       #23 distance to straight
       #24 previous throttle
       #25 delta throttle
       #26 angular velocity
       #27 angular acceleration

       #curve to curve
       #28 linear velocity
       #29 linear acceleration
       #30 drift angular velocity
       #31 drift angular acceleration
       #32 drift angle of car
       #33 angular velocity
       #34 angular acceleration
       #35 angle of curve
       #36 radius of curve
       #37 angle of next curve
       #38 radius of next curve
       #39 previous throttle
       #40 delta throttle

    def __init__(self):
        self._previous_throttle = 0.0
        self._previous_distance = 0.0
        self._previous_angle = 0
        self._previous_piece_index = 0
        self._delta_throttle = 0
        self.sensor = InputSensor()

    def set_previous_throttle(self, value):
        value = min(1, max(0, value))
        self._delta_throttle = value - self._previous_throttle
        self._previous_throttle = value

    def generate(self, bot, carPositions):
        if carPositions == None:
            sys.exit(1)  # we need the car positions
        self.sensor.sense(bot, carPositions)
        pieces = bot.race['track']['pieces']
        lanes = bot.race['track']['lanes']
        lane = None
        for l in lanes:
            if l['index'] == bot.end_lane_index:
                lane = l
        for car in carPositions:
            if car['id']['name'] == bot.name and car['id']['color'] == bot.color:
                piece_index = car['piecePosition']['pieceIndex']
                next_piece_index = (piece_index + 1) % len(pieces)
                piece = pieces[piece_index]
                angle = car['angle'] * math.pi / 180
                linear_velocity = 0
                angular_velocity = angle - self._previous_angle
                distance = car['piecePosition']['inPieceDistance']
                if piece_index == self._previous_piece_index:
                    linear_velocity = distance - self._previous_distance
                else:
                    if 'radius' in pieces[self._previous_piece_index]:
                        linear_velocity = distance - ((pieces[self._previous_piece_index]['angle']/float(360)) * 2\
                                    * math.pi * pieces[self._previous_piece_index]['radius'] - self._previous_distance)
                    elif 'length' in pieces[self._previous_piece_index]:
                        linear_velocity = pieces[self._previous_piece_index]['length'] - \
                                                self._previous_distance + distance
                    else:
                        print("couldn't tell what piece it was")
                        sys.exit(1)
                #straight to straight
                if bot.straight_to_straight:
                    self._input_values[0] = self.sensor.linear_velocity
                    self._input_values[1] = self.sensor.linear_acceleration
                    self._input_values[2] = self.sensor.drift_angle
                    self._input_values[3] = pieces[piece_index]['length'] - distance\
                                              + pieces[next_piece_index]['length']
                    self._input_values[4] = self._previous_throttle
                    self._input_values[5] = self._delta_throttle
 
                #straight to curve
                if bot.straight_to_curve:
                    self._input_values[6] = self.sensor.linear_velocity
                    self._input_values[7] = self.sensor.linear_acceleration
                    self._input_values[8] = self.sensor.drift_angular_velocity
                    self._input_values[9] = self.sensor.drift_angular_acceleration
                    self._input_values[10] = self.sensor.drift_angle
                    self._input_values[11] = pieces[next_piece_index]['angle'] * math.pi / 180
                    self._input_values[12] = pieces[next_piece_index]['radius'] - lane['distanceFromCenter']
                    self._input_values[13] = pieces[piece_index]['length'] - distance 
                    self._input_values[14] = self._previous_throttle
                    self._input_values[15] = self._delta_throttle
                #curve to straight
                if bot.curve_to_straight:
                    self._input_values[16] = self.sensor.linear_velocity
                    self._input_values[17] = self.sensor.angular_acceleration
                    self._input_values[18] = self.sensor.drift_angular_velocity
                    self._input_values[19] = self.sensor.drift_angular_acceleration 
                    self._input_values[20] = self.sensor.drift_angle
                    self._input_values[21] = pieces[piece_index]['angle'] * math.pi / 180
                    self._input_values[22] = pieces[piece_index]['radius'] - lane['distanceFromCenter']
                    self._input_values[23] = (pieces[piece_index]['angle']/ float(360)) * 2 * \
                                                (pieces[piece_index]['radius'] - lane['distanceFromCenter']) * math.pi
                    self._input_values[24] = self._previous_throttle
                    self._input_values[25] = self._delta_throttle
                    self._input_values[26] = self.sensor.angular_velocity
                    self._input_values[27] = self.sensor.angular_acceleration

                #curve to curve
                if bot.curve_to_curve:
                    self._input_values[28] = self.sensor.linear_velocity
                    self._input_values[29] = self.sensor.linear_acceleration
                    self._input_values[30] = self.sensor.drift_angular_velocity
                    self._input_values[31] = self.sensor.drift_angular_acceleration
                    self._input_values[32] = self.sensor.drift_angle
                    self._input_values[33] = self.sensor.angular_velocity
                    self._input_values[34] = self.sensor.angular_acceleration
                    self._input_values[35] = pieces[piece_index]['angle'] * math.pi / 180
                    self._input_values[36] = pieces[piece_index]['radius'] - lane['distanceFromCenter']
                    self._input_values[37] = pieces[next_piece_index]['angle'] * math.pi / 180
                    self._input_values[38] = pieces[next_piece_index]['radius'] - lane['distanceFromCenter']
                    self._input_values[39] = self._previous_throttle
                    self._input_values[40] = self._delta_throttle

                self._previous_angle = angle
                self._previous_piece_index = piece_index
                self._previous_distance = distance
                break    
        return self._input_values

    # compute car position in entire race this should be a fraction
    def _compute_car_position(self, car, race):
        # get all pieces
        _pieces = race['track']['pieces']
        _car_pos = car['piecePosition']

        # get laps
        _laps = race['raceSession']['laps']

        # get car position
        _piece_index = _car_pos['pieceIndex']
        _in_piece_distance = _car_pos['inPieceDistance']
        
        _perimeter = 0
        _car_total = 0
        # add them up
        for i in range(0, len(_pieces)):

            #track car pos
            if _piece_index == i:
                _car_total = _perimeter

            _piece = _pieces[i]
            # straight piece
            if "length" in _piece:
                _perimeter = _perimeter + _piece['length']
            # bend
            elif "radius" in _piece:
                _arc = _piece['radius'] * (_piece['angle'] * math.pi/180)
                _perimeter = _perimeter + _arc

        _car_total = _car_total + _in_piece_distance

        return _car_total/(_perimeter*_laps)

    def get_input_length(self):
        return len(self._input_values)
