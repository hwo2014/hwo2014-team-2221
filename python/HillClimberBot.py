import sys
from InputGenerator import InputGenerator
from ParentBot import ParentBot
import random
import socket
import json
from datetime import datetime

class HillClimberBot(ParentBot):

    def __init__(self, host, port, name, key, randomize = True):
        super(HillClimberBot, self).__init__(host,port, name, key)
        if randomize:
            self.genome = None
        self.generator = InputGenerator()
        self.genome = [0.0025 for i in range(0, len(self.generator._input_values))]


    def on_game_start(self, data):
        super(HillClimberBot, self).on_game_start(data)
    # this should get the scores and associate them with the bot
     # so that we can evolve them
    def on_game_end(self, data):
        print("Race ended")
        sys.stdout.flush()
        for result in data['results']:
            if result['car']['name'] == self.name and result['car']['color'] == self.color:
                if 'laps' in result['result'] and result['result']['laps'] == self.race['raceSession']['laps'] :
                    self.result_laps = result['result']['laps']
                    self.result_ticks = result['result']['ticks']
                    self.result_millis = result['result']['millis']
                    self.result_average_millis = (result['result']['millis'] / \
                                          float(result['result']['laps']))
        for result in data['bestLaps']:
            if result['car']['name'] == self.name and result['car']['color'] == self.color:
                if 'lap' in result['result']:
                    self.result_best_lap = result['result']['lap']
                    self.result_best_lap_ticks = result['result']['ticks']
                    self.result_best_lap_millis = result['result']['millis']
        self.done = True
        self.ping()
 
    def on_car_positions(self, data, tick):
        super(HillClimberBot, self).on_car_positions(data, tick)

        generatedInput = self.generator.generate(self, data)
        throttle = self.apply_genomes(generatedInput)
        
        # need to determine if switch or turbo are applicable
        self.throttle(throttle)
        self.generator.sensor.set_throttle(throttle)
       
    # this is where we apply the genomes to our input
    def apply_genomes(self, generated_input):
        # cap the values between 0 and 1
        # if not initialised, set it to a value such that we start with 0.5 throttle
        product = [ self.genome[i] * generated_input[i] for i in range(0, len(generated_input))]
        
        s = 0.1
        if self.straight_to_straight:
            s = sum(product[0:6])    
        elif self.straight_to_curve:
            s = sum(product[6:16])
        elif self.curve_to_straight:
            s= sum(product[16:28])
        elif self.curve_to_curve:
            s = sum(product[28:41])
       
        s = min(1, max( 0, s))
        return s

