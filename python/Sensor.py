import sys
import math

class Sensor(object):
    can_switch = False

    straight_to_straight_to_straight = False
    straight_to_straight_to_curve = False
    straight_to_curve_to_straight = False
    straight_to_curve_to_curve = False
    curve_to_straight_to_straight = False
    curve_to_straight_to_curve = False
    curve_to_curve_to_straight = False
    curve_to_curve_to_curve = False

    _previous_piece_index = 0
    _previous_throttle = 0
    _previous_drift_angle = 0
    _previous_distance = 0
    _previous_linear_velocity = 0
    _previous_drift_angular_velocity = 0
    _previous_angle = 0
    _previous_angular_velocity = 0

    drift_angle = 0
    delta_throttle = 0
    drift_angular_velocity = 0
    linear_velocity = 0
    drift_angular_acceleration = 0
    linear_acceleration = 0
    angular_velocity = 0
    angular_acceleration = 0

    def __init__(self):
        pass

    def log(self):
        print("Begin sensor print")
        print("drift_angle :" + str(self.drift_angle))
        print("delta_throttle :" + str(self.delta_throttle))
        print("linear_velocity :" + str(self.linear_velocity))
        print("drive_angular_velocity" + str(self.drift_angular_velocity))
        print("drift_angular_accelartion :" + str(self.drift_angular_acceleration))
        print("linear_acceleration :" + str(self.linear_acceleration))
        print("angular_velocity :" + str(self.angular_velocity))
        print("angular_acceleration :" + str(self.angular_acceleration))
        print("End sensor print")

    def set_throttle(self, throttle):
        self.delta_throttle = throttle - self._previous_throttle
        self._previous_throttle = throttle

    def sense(self, bot, carPositions):
        self._get_states(bot, carPositions)
        if carPositions == None:
            sys.exit(1)  # we need the car positions
        
        pieces = bot.race['track']['pieces']
        lanes = bot.race['track']['lanes']
        lane = None
        for l in lanes:
            if l['index'] == self.end_lane_index:
                lane = l
        for car in carPositions:
            if car['id']['name'] == bot.name and car['id']['color'] == bot.color:
                piece_index = car['piecePosition']['pieceIndex']
                next_piece_index = (piece_index + 1) % len(pieces)
                piece = pieces[piece_index]
                self.drift_angle = car['angle'] * math.pi / 180
                self.drift_angular_velocity = self.drift_angle - self._previous_drift_angle
                self.drift_angular_acceleration = self.drift_angular_velocity - self._previous_drift_angular_velocity
                distance = car['piecePosition']['inPieceDistance']
                if piece_index == self._previous_piece_index:
                    self.linear_velocity = distance - self._previous_distance
                    if "radius" in piece:
                        self.angular_velocity = self.linear_velocity / piece['radius']
                        self.angular_acceleartion = self.angular_velocity - self._previous_angular_velocity
                    else:
                        self.angular_velocity = 0
                        self.angular_acceleration = 0

                else:
                    self.angular_velocity = 0
                    self.angular_acceleration = 0
                    if "radius" in pieces[self._previous_piece_index]:
                        self.linear_velocity = distance + ((pieces[self._previous_piece_index]["angle"]/float(360)) * 2\
                                    * math.pi * pieces[self._previous_piece_index]['radius']) - self._previous_distance
                    elif "length" in pieces[self._previous_piece_index]:
                        self.linear_velocity = pieces[self._previous_piece_index]['length'] - \
                                                self._previous_distance + distance
                    else:
                        print("couldn't tell what piece it was")
                        sys.exit(1)
               

                self.linear_acceleration = self.linear_velocity - self._previous_linear_velocity
                self._previous_linear_velocity = self.linear_velocity
                self._previous_drift_angular_velocity = self.drift_angular_velocity
                self._previous_angular_velocity = self.angular_velocity
                self._previous_angular_acceleration = self.angular_acceleration
                self._previous_drift_angle = self.drift_angle
                self._previous_piece_index = piece_index
                self._previous_distance = distance
                break

    def _get_states(self, bot, data):
        for car in data:
            if car['id']['name'] == bot.name and car['id']['color'] == bot.color:
                pieces = bot.race['track']['pieces']
                car_piece_index = car['piecePosition']['pieceIndex']
                if "switch" in pieces[car_piece_index] and pieces[car_piece_index]['switch']:
                    if car['piecePosition']['lane']['startLaneIndex'] == car['piecePosition']['lane']['endLaneIndex']:
                        if car['piecePosition']['lane']['startLaneIndex'] < len(bot.race['track']['lanes']) -1:
                            self.switch_right = True
                        else:
                            self.switch_right = False
                        if car['piecePosition']['lane']['startLaneIndex'] >0:
                            self.switch_left = True
                        else:
                            self.switch_left = False
                        self.can_switch = True
                    else:
                        self.can_switch = False 
                        self.switch_left = False
                        self.switch_right = False
                else:
                    self.can_switch = False
                    self.switch_left = False
                    self.switch_right = False
                car["canSwitch"] = self.can_switch

                # lane of the car
                self.start_lane_index = car['piecePosition']['lane']['startLaneIndex']
                self.end_lane_index = car['piecePosition']['lane']['endLaneIndex']
                    
                # determine the position of the car
                next_index = (car_piece_index + 1) % len(pieces)
                next_next_index = (car_piece_index + 2) % len(pieces)
                if "length" in pieces[car_piece_index]:
                    if "length" in pieces[next_index]:
                        self.straight_to_straight_to_straight = True if "length" in pieces[next_next_index] else False
                        self.straight_to_straight_to_curve = True if "radius" in pieces[next_next_index] else False
                    elif "radius" in pieces[next_index]:
                        self.straight_to_curve_to_straight = True if "length" in pieces[next_next_index] else False
                        self.straifht_to_curve_to_curve = True if "radius" in pieces[next_next_index] else False
                else:
                    if "length" in pieces[next_index]:
                        self.curve_to_straight_to_straight = True if "length" in pieces[next_next_index] else False
                        self.curve_to_straight_to_curve = True if "radius" in pieces[next_next_index] else False
                    elif "radius" in pieces[next_index]:
                        self.curve_to_curve_to_straight = True if "length" in pieces[next_next_index] else False
                        self.curve_to_curve_to_curve = True if "radius" in pieces[next_next_index] else False
